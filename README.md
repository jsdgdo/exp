

# TP 2 - NodeJS estático

## Integrantes

* Carlos Arce
* José Delgado

## Pasos que llevamos a cabo

**Paso 1:** Creamos el repositorio **exp**  

**Paso 2:** Descargamos e instalamos NodeJS

- Link de descarga: https://nodejs.org/en/ 

- Verificamos si se ha instalado y que version ejecutando `node -v` y `npm -v`


**Paso 3:** Instalamos el framework Express `npm install express`

**Paso 4:** Creamos el proyecto Node con: `npm init`

**Paso 5:** Creamos el archivo .gitignore para asegurarnos de no incluir la carpeta node_modules dentro del repositorio GIT.

**Paso 6:** Clonamos la Revisión 1 a nuestro repositorio.

**Paso 7:** Creamos un nuevo servidor Web. `const app = express();`

**Paso 8:** Utilizamos el framework Express, configuramos un servidor Web en el puerto 3000 y creamos las rutas para la Revision 1.

**Paso 9:**  Utilizando express agregamos los recursos de la revision-1 `app.use(express.static('public'))`


## Pasos para correr localmente

**Paso 1:** Clonar el presente repositorio con la opción `--recurse-submodules`

**Paso 2:** Si es que en la máquina no está instalada Node, [instalar de acuerdo a la plataforma](https://nodejs.org/en/download/package-manager/).

**Paso 3:** Correr `npm install` en la raíz del proyecto.

**Paso 4:** Correr `npm run dev` para levantar el servidor local en el puerto `3000`.

**Paso 5:** En su navegador de preferencia visite [http://localhost:3000](http://localhost:3000)
